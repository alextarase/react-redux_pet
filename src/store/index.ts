import {
  configureStore,
  //  applyMiddleware
} from "@reduxjs/toolkit";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { rootReducer } from "./reducers/index";
import { Middleware, getDefaultMiddleware } from "@reduxjs/toolkit";

export const store = configureStore({
  reducer: rootReducer,
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(thunk),
});

// export const store = createStore(rootReducer, applyMiddleware(thunk));
